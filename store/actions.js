/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

'use strict';

const actions = {
  nuxtServerInit({commit}, {req, redirect}) {
  },
  RESTART_OS({commit}) {
    commit('RESTART_OS', true);
  }
};

export default actions
