module.exports = {
  router: { // https://nuxtjs.org/api/configuration-router
    linkActiveClass: 'active',
    linkExactActiveClass: 'exact-active',
    scrollBehavior: null,
    middleware: [
      'i18n'
    ],
    extendRoutes(routes, resolve) {
      routes.push({
        name: 'custom',
        path: '*',
        component: resolve(__dirname, `pages/404.vue`)
      });
    }
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'Server Panel',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: ''}
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
    ]
  },
  /*
  ** Global CSS
  */
  css: [
    '~/assets/css/main.css',
    // '~/assets/css/msp-style.css',
    '~/assets/css/msp-general.css',
    '~/assets/css/msp-components.css',
    'animate.css/animate.min.css'
  ],
  /*
  ** Add axios globally
  */
  build: {
    vendor: [
      'axios',
      'bootstrap-vue'
    ],
    /*
    ** Run ESLINT on save
    */
    extend(config, ctx) {
      if (ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  modules: [
    'bootstrap-vue/nuxt',
    '@nuxtjs/font-awesome'
  ],
  plugins: [
    '~/plugins/api',
    '~/plugins/i18n.js',
    {src: '~/plugins/faye', ssr: false},
    {src: '~/plugins/notify', ssr: false}
  ],

  loading: {
    color: '#C21A26'
  }
};
