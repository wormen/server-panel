/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

module.exports = (locale) => {
  let obj = {};
  for (let type of [
    'alert', 'auth', 'btn', 'errcode', 'head', 'label',
    'moduleExe', 'placeholder', 'pathList', 'stream',
    'select', 'terminalList', 'title'
  ]) {
    obj[type] = require(`./${locale}/${type}.json`)
  }
  return obj;
};
