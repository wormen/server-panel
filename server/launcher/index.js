/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

import {existsSync} from 'fs';
import path from 'path';
import {spawn} from 'child_process';
import Fs from '../libs/Fs';

export default class Index {

  /**
   * Запуск служебных сервисов
   * @returns {Promise}
   */
  static startServices() {
    return new Promise(async (resolve, reject) => {
      const dir = path.resolve(ROOT_DIR, 'workers'); // каталог, где лежат сервисы
      if (existsSync(dir)) {
        const list = await Fs.dirList(dir).catch(reject); // получаем список сервисов
        if (Array.isArray(list)) {
          list.forEach(item => {
            if (item.includes('.service')) {
              launchService(item);
            }
          });
        }
      } else {
        resolve();
      }
    });
  }
}

function launchService(item) {
  let file = path.resolve(dir, item);
  let app = spawn('node', [file], { // запускаем сервис
    env: process.env,
    detached: true
  });

  // перезапускаем сервис, в случае ошибки
  app.on('close', (code) => {
    if (code > 0) {
      launchService(item);
    }
  });
}
