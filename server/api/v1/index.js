/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

import {Router} from 'express'

import backups from './backups';
import domains from './domains';
import email from './email';
import ftp from './ftp';
import setting from './setting';

const router = Router();

router.use(backups);
router.use(domains);
router.use(email);
router.use(ftp);
router.use(setting);

export default router
