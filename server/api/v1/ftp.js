import {Router} from 'express';
import Ftp from '../../libs/os/Ftp';

const router = Router();
const MU = '/ftp';

router.get(`${MU}/getList`, (req, res) => {
  (new Ftp()).getUsers()
    .then(list => res.json(list))
    .catch(e => res.sendStatus(500));
});

router.post(`${MU}/save`, (req, res) => {
  (new Ftp()).saveUsers(req.body.users)
    .then(() => res.sendStatus(201))
    .catch(e => res.sendStatus(500));
});


export default router;
