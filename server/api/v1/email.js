import {Router} from 'express';
import Email from '../../libs/os/Email';

const router = Router();
const MU = '/email';

router.get(`${MU}/getList`, (req, res) => {
  (new Email()).getList()
    .then(list => res.json(list))
    .catch(e => res.sendStatus(500));
});

router.get(`${MU}/getItem/:name`, (req, res) => {
  (new Email()).viewFile(req.params.name)
    .then(data => res.json(data))
    .catch(e => res.sendStatus(500));
});

router.delete(`${MU}/remove/:name`, (req, res) => {
  (new Email()).remove(req.params.name)
    .then(() => res.send(200))
    .catch(e => res.sendStatus(500));
});

export default router;
