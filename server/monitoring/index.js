/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

import {EventEmitter} from 'events';
import Cpu from './Cpu';
import Drives from './Drives';
import Memory from './Memory';

export default class Monitoring extends EventEmitter {
  constructor() {
    super();
    this._init();
  }

  _init(){
    (new Cpu()).on('data', (data) => {
      this.emit(`cpu`, data);
    });

    (new Memory()).on('data', (data) => {
      this.emit(`memory`, data);
    });

    (new Drives()).on('data', (data) => {
      this.emit(`disk`, data);
    });
  }
}