/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 https://www.npmjs.com/package/log4js
 */

import log4js from 'log4js';

log4js.configure({
  appenders: {
    console: {
      type: 'console',
      layout: {
        type: 'pattern',
        pattern: '%[%d{yyyy-MM-dd hh.mm.ss.SSS} [%p]%] %m'
      }
    }
  },
  categories: {
    default: {
      appenders: ['console'],
      level: process.env.DEBUG_LEVEL || 'info'
    }
  }
});

let log = log4js.getLogger();

export default class Log {

  static info(...args) {
    log.info(...args)
  }

  static trace(...args) {
    log.trace(...args)
  }

  static debug(...args) {
    log.debug(...args)
  }

  static warn(...args) {
    log.warn(...args)
  }

  static error(...args) {
    log.error(...args)
  }

  static fatal(...args) {
    log.fatal(...args)
  }
};
