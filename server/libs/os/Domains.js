/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

import punycode from 'punycode';
import path from 'path';
import Fs from '../Fs';
import OpenServer from "./index";

export default class Domains extends OpenServer {
  constructor() {
    super()
  }

  getListWWW() {
    return new Promise(async (resolve, reject) => {
      const list = await Fs.dirList(this.wwwPath).catch(reject);

      resolve({
        www: this.wwwPath,
        list
      });
    });
  }

  save(domain){
    return Fs.emptyDir(path.resolve(this.wwwPath, punycode.toASCII(domain)));
  }

  remove(domain) {
    return Fs.remove(path.resolve(this.wwwPath, domain));
  }
}
