/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 https://github.com/louischatriot/nedb
 */

import path from 'path';
import Sequelize from 'sequelize'; // http://docs.sequelizejs.com/

let DB = null;

export default class Database {
  constructor() {
    this._ext = '.db';
    this._init();
  }

  _init() {
    const storage = path.resolve(DATA_DIR, `osp${this._ext}`);
    DB = new Sequelize('os-panel', null, null, {
      dialect: 'sqlite',
      logging: false,
      define: {
        underscored: true,
        freezeTableName: true,
        charset: 'utf8',
        dialectOptions: {
          collate: 'utf8_general_ci'
        },
        timestamps: false
      },
      storage,
      operatorsAliases: Sequelize.Op // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
    });
  }

  // --------- функции БД ---------
  static query(sql, opts) {
    return DB.query(sql, Object.assign({raw: true, plain: true}, opts));
  }

  static upsert(model, data = {}, where = {}) {
    return new Promise((resolve, reject) => {
      model.create(data)
        .then(resolve)
        .catch(e => {
          if (e.name === 'SequelizeUniqueConstraintError') {
            return model.update(data, {where})
              .then(resolve)
              .catch(reject)
          } else {
            reject(e);
          }
        });
    });
  }

  // --------- Модели ---------

  static get Setting() {
    const model = DB.define('setting', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      key: {
        type: Sequelize.TEXT,
        allowNull: false,
        unique: true
      },
      value: {
        type: Sequelize.TEXT,
        allowNull: false
      }
    }, {
      updateOnDuplicate: true,
      tableName: 'setting'
    });

    model.sync();

    return model;
  }
}

new Database();
