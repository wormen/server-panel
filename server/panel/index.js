/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 Класс для служебного функционала панели
 */

import {EventEmitter} from 'events';
import {getKey} from '../libs/os';

export default class Panel extends EventEmitter {
  constructor() {
    super();
  }

  checkUpdate() {
    return getKey('general').then(val => {
      return val.checkupdatePanel;
    });
  }
}