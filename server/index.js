import '../_constants';
import http from 'http';
import express from 'express';
import {Nuxt, Builder} from 'nuxt';
import Faye from './libs/Faye';
import Fs from './libs/Fs';
import Monitoring from './monitoring';
import Process from './libs/Process';
import {Server as MqttServer} from './mqtt';
import Launcher from './launcher';
import OpenServer from './libs/os';

import api from './api/v1'

const
  cookieParser = require('cookie-parser'),
  bodyParser = require('body-parser');

const app = express();
const host = process.env.HOST || '127.0.0.1';
const port = process.env.PORT || 11000;

global.$Faye = new Faye();

app
  .set('port', port)
  .use(bodyParser.json())
  .use(bodyParser.urlencoded({extended: false}))
  .use(cookieParser());

// Import API Routes
app.use('/api/v1', api);

// Import and Set Nuxt.js options
let config = require('../nuxt.config.js');
config.dev = !(process.env.NODE_ENV === 'production');

if (config.dev) {
  process.env.DEBUG_LEVEL = 'debug';
}

// Init Nuxt.js
const nuxt = new Nuxt(config);

// Build only in dev mode
if (config.dev) {
  const builder = new Builder(nuxt);
  builder.build()
}

// Give nuxt middleware to express
app.use(nuxt.render);

(async () => {
  await Promise.all([
    await Fs.emptyDir(DATA_DIR)
  ]);

  // зачитываем конфиг
  console.log('Loading config ...');
  await OpenServer.init().then(() => {
    console.log('Config loaded');
  }).catch(e => {
    console.error('Error loaded config', e);
  });


  const server = http.createServer(app).listen(port, () => {
    console.log('HTTP Server listening on ' + host + ':' + port);
  });

  (new MqttServer()).listen().on('ready', (port) => {
    console.log('MQTT Server listening on ' + host + ':' + port);
  });

  // Faye сервер
  $Faye.attach(server);

  const mn = new Monitoring();
  mn.on('cpu', data => {
    $Faye.sendClient(`system/cpu`, data);
    $Faye.sendClient(`system/env`, process.env.OPEN_SERVER);
  });
  mn.on('disk', data => {
    $Faye.sendClient(`system/drives`, data);
  });
  mn.on('memory', data => {
    $Faye.sendClient(`system/memory`, data);
  });

  const _Process = new Process();
  _Process.setAutoPID(process.env.OPEN_SERVER.PID || 0);
  _Process.autoList(true);
  _Process.on('autoList:data', list => {
    let obj = {
      isLaunch: list.length > 1 && list[0].hasOwnProperty('pid'),
      list
    };
    _Process.setAutoPID(process.env.OPEN_SERVER.PID);
    $Faye.sendClient(`system/processes`, obj);
  });

  // Launcher.startServices();
})();
