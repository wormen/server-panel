/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 https://www.npmjs.com/package/uuid
 */

const uuidv1 = require('uuid/v1');
const uuidv4 = require('uuid/v4');
const uuidv5 = require('uuid/v5');

export default class UUID {
  /**
   * Генерируем на основе Timestamp
   * @param options - данные на осное которых генерируется хэш
   *   <Array> node - Идентификатор узла как массив из 6 байтов (на 4.1.6). По умолчанию: произвольно сгенерированный идентификатор
   *   clockseq <Number between 0 - 0x3fff> - Временная последовательность RFC. По умолчанию: используется внутренне поддерживаемый clockseq
   *   msecs <Number | Date> - Время в миллисекундах с момента unix Epoch. По умолчанию: используется текущее время
   *   nsecs <Number between 0-9999> - дополнительное время, в 100-наносекундных единицах. Игнорируется, если msecs не указывается. Значение по умолчанию: используется внутренний счетчик uuid, согласно 4.2.1.2
   * @param buffer <Array | Buffer> - Массив или буфер, где должны быть записаны байты UUID
   * @param offset <Number> - Начальный индекс в буфере, с которого нужно начинать писать
   * @returns {*}
   */
  static v1(options, buffer, offset) {
    return uuidv1(options, buffer, offset);
  }

  /**
   * Генерируем случайный UUID
   * @param options - параметры для применения к UUID
   *   random <Number[16]> - массив из 16 чисел, для использования вместо случайно генерируемых значений
   *   rng <Function> - Функция случайных чисел, возвращающая массив байтов
   * @param buffer <Array | Buffer> - Массив или буфер, где должны быть записаны байты UUID
   * @param offset <Number> - Начальный индекс в буфере, с которого нужно начинать писать
   * @returns {*}
   */
  static v4(options, buffer, offset) {
    return uuidv4(options, buffer, offset);
  }

  /**
   * Генерируем значение на основе Namespace
   * @param name <String | Array> - имя для создания UUID
   * @param namespace <String | Array> - пространство имен UUID как String или Array [16]
   * @param buffer <Array | Buffer> - Массив или буфер, где должны быть записаны байты UUID
   * @param offset <Number> - Запуск индекса в буфере, с которого нужно начинать писать. По умолчанию = 0
   * @returns {*}
   */
  static v5(name, namespace, buffer, offset) {
    return uuidv5(name, namespace, buffer, offset);
  }

  static namespace(name) {
    let ns = this.v4();
    return this.v5(name, ns);
  }
};
