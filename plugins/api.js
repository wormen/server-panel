/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

'use strict';

import Vue from 'vue';
import ApiPlugin from '~/core/api';

Vue.use(ApiPlugin);

export default ({app, store}) => {
  app.$API = Vue.prototype.$API;
}
