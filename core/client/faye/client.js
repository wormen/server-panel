/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const Faye = require('faye');

export default class FayeClient {
  constructor() {
    this.client = null;
    this.isOnline = false;
  }

  connectInit() {
    this.connect();
  }

  connect() {
    let url = process.env.API_HOST;
    url = '';
    this.client = new Faye.Client(`${url}/client`, {
      timeout: 60
    });

    this.client.on('transport:down', () => {
      this.isOnline = false;
    });

    this.client.on('transport:up', () => {
      this.isOnline = true;
    });
  }

  setChannel(channel) {
    return '/' + ['client', channel].join('/');
  }

  publish(channel, data) {
    // if (this.client && !this.isOnline) {
    //   return false;
    // }

    return this.client.publish(this.setChannel(channel), data);
  }

  subscribe(channel, cb) {
    // if (this.client && !this.isOnline) {
    //   return false;
    // }

    if (String(channel).includes('*')) {
      return this.client.subscribe(this.setChannel(channel)).withChannel((cn, message) => {
        cb(cn, message);
      });
    }

    return this.client.subscribe(this.setChannel(channel), cb);
  }
}
