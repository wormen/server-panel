/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

export default {
  install(Vue, options) {
    if (!Vue.prototype.hasOwnProperty('$notify')) {
      /**
       * Всплывающие уведомления, которые выводятся наверху
       * @param type
       * @param message
       */
      Vue.prototype.$notify = function (type, message) {
        this.$root.$emit('NOTIFY', {type, message});
      };
    }
  }
}
