/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

'use strict';

import axios from '~/plugins/axios';
import {isJson, isString} from '~/plugins/libs';

let locale = 'ru';

class Api {
  constructor() {
    this.url = ``;
  }

  Error(err) {
    if (err == null) return false;
    console.error(err);
  }

  get HOST() {
    return process.env.API_HOST || '';
  }

  fullUrl(url, v = `v1`) {
    this.url = [this.HOST, 'api', v].join('/') + String(url).trim();
    return this.url;
  }

  customUrl(url) {
    return [this.HOST, url].join('');
  }

  setLocale(val) {
    locale = val;
  }

  getHeaders(list = {}) {
    // list['X-Panel'] = process.env.PANEL;
    list[`X-Locale`] = locale; // локализация пользователя
    return list;
  }

  get defaultOpts() {
    return {
      headers: this.getHeaders()
    }
  }

  request(opts = {}) {
    opts.headers = this.getHeaders(opts.headers || {});

    return axios.request(opts);
  }

  /**
   * Разбор ответа
   * @param res
   * @returns {string}
   */
  response(res) {
    let data = ``;

    if (res.hasOwnProperty('data')) {
      data = res.data;
    } else {
      data = res.body;
    }

    if (isString(data) && isJson(data)) {
      return JSON.parse(data);
    }

    return data;
  }
}

export default Api;
