/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

import axios from '~/plugins/axios';
import Api from './Api';

export default class Ftp extends Api {
  constructor() {
    super();
    this.url = super.fullUrl(`/ftp`);
  }

  getList() {
    return axios.get(`${this.url}/getList`, super.defaultOpts)
      .then(super.response);
  }

  save(data) {
    return axios.post(`${this.url}/save`, data, super.defaultOpts)
      .then(super.response);
  }
}
