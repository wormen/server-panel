/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

import axios from '~/plugins/axios';
import Api from './Api';

export default class Email extends Api {
  constructor() {
    super();
    this.url = super.fullUrl(`/email`);
  }

  getList() {
    return axios.get(`${this.url}/getList`, super.defaultOpts)
      .then(super.response);
  }

  getItem(fileName) {
    return axios.get(`${this.url}/getItem/${fileName}`, super.defaultOpts)
      .then(super.response);
  }

  remove(fileName) {
    return axios.delete(`${this.url}/remove/${fileName}`, super.defaultOpts)
      .then(super.response);
  }
}
