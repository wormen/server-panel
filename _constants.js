/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

const path = require('path');

global.ROOT_DIR = path.resolve(__dirname);
global.DATA_DIR = path.join(ROOT_DIR, 'data');
global.TOOLS_DIR = path.join(ROOT_DIR, 'tools');
global.LOGS_DIR = path.join(ROOT_DIR, 'logs');
global.SCRIPTS_DIR = path.join(ROOT_DIR, 'scripts');

process.env = Object.assign({}, process.env, {
  OPEN_SERVER: {
    EXEC: '', // название исполняемого файла
    ROOT_DIR: '',
    BACKUP_DIR: '',
    VALID_ROOT_DIR: false
  }
});
